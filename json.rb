require 'json'

count_keys = Hash.new 0; ks = []

def get_em(in_hash, key_prefix='')
  keys = []; in_hash.each { |k, v| v.instance_of?(Hash) ? keys += get_em(v, key_prefix + k + '.') : keys << key_prefix + k }; keys
end

Dir["im_jsons/*"].each do |file|
  File.open(file, 'r').each do |row|
    ks += get_em JSON.parse(row)
  end
end

ks.each { |v| count_keys[v.gsub(/basket.\d+/, 'basket')] += 1 }
count_keys.sort_by { |k, v| v }.to_h.each {|k, v| puts "#{k} : #{v}"}